package cl.salemlabs.firebaseejemplo;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.preference.EditTextPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText etUser;
    private EditText etPass;
    private Button btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUser = (EditText)findViewById(R.id.et_user);
        etPass = (EditText)findViewById(R.id.et_pass);
        btnAceptar = (Button) findViewById(R.id.btn_aceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLoginSuccess(etUser.getText().toString(),
                        etPass.getText().toString())){
                    cambiarActivityMenu();
                }else{
                    mensajeDeError();
                }
            }
        });
    }

    private void mensajeDeError() {
        Toast.makeText(this, "Error autentificación", Toast.LENGTH_LONG).show();
    }

    private void cambiarActivityMenu() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    protected EditText getEtUser() {
        return etUser;
    }

    protected EditText getEtPass() {
        return etPass;
    }

    protected Button getBtnAceptar() {
        return btnAceptar;
    }

    public boolean isLoginSuccess(String user, String pass) {
        boolean userValid = false;
        if(user.equals("admin") && pass.equals("admin")){
            userValid = true;
        }
        return userValid;
    }
}
