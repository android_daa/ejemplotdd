package cl.salemlabs.firebaseejemplo;

import android.content.Intent;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowHandler;
import org.robolectric.shadows.ShadowToast;

import static org.assertj.android.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by hardroidlabs on 23-06-17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LoginActivityTest {

    private LoginActivity loginActivity;
    private ActivityController<LoginActivity> loginActivityController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loginActivityController = Robolectric.buildActivity(LoginActivity.class).setup();
        loginActivity = loginActivityController.get();
    }

    @Test
    public void debeCrearVistasLogin() throws Exception {
        assertThat(loginActivity.getEtUser()).isNotNull();
        assertThat(loginActivity.getEtPass()).isNotNull();
        assertThat(loginActivity.getBtnAceptar()).isNotNull();
    }

    @Test
    public void debeValidarLoginCorrecto() throws Exception {
        assertTrue(loginActivity.isLoginSuccess("admin","admin"));
    }

    @Test
    public void debeValidarLoginIncorrecto() throws Exception {
        assertFalse(loginActivity.isLoginSuccess("",""));
    }

    @Test
    public void debeRealizarClickDelBotonOK() throws Exception {
        Button btnEntrar = loginActivity.getBtnAceptar();
        loginActivity.getEtPass().setText("admin");
        loginActivity.getEtUser().setText("admin");
        btnEntrar.performClick();
        Intent intent = Shadows.shadowOf(loginActivity).peekNextStartedActivity();
        assertThat(intent)
                .isNotNull()
                .hasComponent(loginActivity.getApplicationContext(), MainActivity.class);
    }

    @Test
    public void debeRealizarClickDelBotonErrorToast() throws Exception {
        Button btnEntrar = loginActivity.getBtnAceptar();
        loginActivity.getEtPass().setText("admin1");
        loginActivity.getEtUser().setText("admin1");
        btnEntrar.performClick();
        assertEquals( ShadowToast.getTextOfLatestToast(), "Error autentificación");
    }


}
